#+OPTIONS: toc:nil num:nil H:4 tex:t
#+LATEX_HEADER: \usepackage[margin=0.95in]{geometry}

#+AUTHOR: Shabaz Badshah
#+TITLE: Error Detection
#+DATE: Wed Jan 17, 2018

* Error Correction Strategies
- detect error (also called retransmiting) , or Automatic Repeat reQuest (ARQ)
- error correcting codes, or Forward Error Correction (FEC)
- you need to detect before you can correct
* Error Detection or Error Correction
- Most data networks use error detection
- Detection
   - Requires smaller overhead/bits
   - Requires simpler/less processing
- Advantages of Error Correction
  - Reduces number of retransmissions 
* Hamming Distance 
- turns data of n bits into codewords of $n+k$ bits
- Hamming distance of a code is the smallest number of bits difference that turn any one codeword into another 
  - e.x. code 000 for 0, 111 for 1, Hamming distance is 3
- the codeword is XORd with the next codeword until you get your final number
  - the last number (counting the 1s) will tell you how many bits you can correct
- Detecting a d-bit error: $HD \ge d+1$
- Correcting a d-bit error: $HD \ge 2d+1$ 
* Parity Checking
- given n bits, if there are an *odd* amount of 1s in the message, then add a 1 to the end of the message
  - e.x. 0110010 (has three 1s) $\rightarrow$ 01100101 (added the 1 at the end)
- computing a detection is easy, just XOR all the input bits
- will detect only odd number of bit errors, but not even 
- *does not correct any errors*
* 2D Parity
- helps detect and correct 
- adds a parity in each of the rows and a parity in each of the columns
- corret all 1 bit errors
* Checksum
- used in internet protocols (IP, ICMP, TCP, UDP)
- add up the data and send it along with the sum
- checksum is the 1s complement of the 1s complement sum (done 16-bits at a time since TCP/UDP use 16-bit checksums)
