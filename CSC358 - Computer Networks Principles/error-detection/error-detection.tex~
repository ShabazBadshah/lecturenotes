% Created 2018-01-17 Wed 09:48
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{float}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{marvosym}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{hyperref}
\tolerance=1000
\usepackage[margin=0.95in]{geometry}
\author{Shabaz Badshah}
\date{Wed Jan 17, 2018}
\title{Error Detection}
\hypersetup{
  pdfkeywords={},
  pdfsubject={},
  pdfcreator={Emacs 24.5.1 (Org mode 8.2.10)}}
\begin{document}

\maketitle

\section*{Error Correction Strategies}
\label{sec-1}
\begin{itemize}
\item detect error (also called retransmiting) , or Automatic Repeat reQuest (ARQ)
\item error correcting codes, or Forward Error Correction (FEC)
\item you need to detect before you can correct
\end{itemize}
\section*{Error Detection or Error Correction}
\label{sec-2}
\begin{itemize}
\item Most data networks use error detection
\item Detection
\begin{itemize}
\item Requires smaller overhead/bits
\item Requires simpler/less processing
\end{itemize}
\item Advantages of Error Correction
\begin{itemize}
\item Reduces number of retransmissions
\end{itemize}
\end{itemize}
\section*{Hamming Distance}
\label{sec-3}
\begin{itemize}
\item turns data of n bits into codewords of $n+k$ bits
\item Hamming distance of a code is the smallest number of bits difference that turn any one codeword into another 
\begin{itemize}
\item e.x. code 000 for 0, 111 for 1, Hamming distance is 3
\end{itemize}
\item the codeword is XORd with the next codeword until you get your final number
\begin{itemize}
\item the last number (counting the 1s) will tell you how many bits you can correct
\end{itemize}
\item Detecting a d-bit error: $HD \ge d+1$
\item Correcting a d-bit error: $HD \ge 2d+1$
\end{itemize}
\section*{Parity Checking}
\label{sec-4}
\begin{itemize}
\item given n bits, if there are an \textbf{odd} amount of 1s in the message, then add a 1 to the end of the message
\begin{itemize}
\item e.x. 0110010 (has three 1s) $\rightarrow$ 01100101 (added the 1 at the end)
\end{itemize}
\item computing a detection is easy, just XOR all the input bits
\item will detect only odd number of bit errors, but not even
\item \textbf{does not correct any errors}
\end{itemize}
% Emacs 24.5.1 (Org mode 8.2.10)
\end{document}
