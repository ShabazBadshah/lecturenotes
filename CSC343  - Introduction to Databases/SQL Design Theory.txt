SQL Design Theory
	- Helps create schemas which do not create redudent information
	- Helps create a efficient DB 
	- DDT helps systematically improve DB design
	
	- Getting a DB to Normal Form is what we mean by 'efficient'
	
	[Functional Dependencies]
		- Restrict what can go in a relation
		- Determined by the knowledge of the domain
		- What determines what?
		- Definition:
			If R is a relation and X,Y are two attributes of R, then the Y funcitonally depends on X
			
			Denoted by: X->Y
			
			Y depends on the value of X
		
		- Edge Cases:
			- A->A always holds
			- AB->A always holds
			- If X->Y and X <intersects> Y = Y, then X->Y is trival
			
		
	[Anomalies]
		- Insertion: can't insert data into DB because the other data that this new insertion 
					 relies on is not in the DB
		- Update: when some data that needs to be updated is not changed, other items in the DB
				  that rely on the DB will not work
		- Deletion: when deletion of unwanted data causes some information you still wanted to keep 
					to get deleted as well
	
	[Splitting and Combining FDs]
		- E.g. Splitting FD
			- When split the RHS of the FD and combine it with the LHS, from the FD above
			
			A1,A2,A3,...,An->B1,B2,B3,...,Bm 
			<split>
			A1,A2,A3,...,An->B1
			A1,A2,A3,...,An->B2
			A1,A2,A3,...,An->B3
			...
			A1,A2,A3,...,An->Bm
			
			- There's no safe way to split 
			
		- E.g. Combining FD
			A1,A2,A3,...,An->B1
			A1,A2,A3,...,An->B2
			A1,A2,A3,...,An->B3
			...
			A1,A2,A3,...,An->Bm
			<combine>
			A1,A2,A3,...,An->B1,B2,B3,...,Bm
	
	[Transitive Rule]
		- If A->B hold and B->C hold, then A->C holds
		
	[FDs and Keys]
		- Given K a set of attributes on R
		- K is a superkey for iff it functionally determines all of R
		
	[Closure]
		- E.g. L={A->B, B->CD, D->E, C->DEF}
			
			A+={A->B, B->CD}
			C+={C->DEF}
			
	[Minimal Basis]
		- The FD that allows us to create all other combinations of FDs in the given FD set

	[Decomposition and BCNF]
		- Used to eliminate anomalies 		
		- BCNF is the process we use to decompose a relation in order to eliminate anomalies
		- A relation R is in BCNF iff A1,A2,...,An->B1,B2,...,Bn then {A1,A2,...,An} is the 
		  super key of R
		- To be in BCNF the left side of the FD must be the super-key of the table (does not have to
		  be minimal super-key)